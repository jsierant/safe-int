/*
    Safe Integer library
    Copyright (C) 2011  Jarosław Sierant jaroslaw.sierant@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"

namespace safe_int
{
namespace ut
{

class CalculatorTest : public ::testing::Test
{
protected:
	CalculatorTest() { }
	virtual ~CalculatorTest() { }
	virtual void SetUp() { }
	virtual void TearDown() { }
};


TEST_F(CalculatorTest, test1)
{
}

} // namespace ut
} // namespace safe_int
// kate: indent-mode cstyle; indent-width 3; replace-tabs off; tab-width 3;
