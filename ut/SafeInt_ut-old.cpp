/*
    Safe Integer library
    Copyright (C) 2011  Jarosław Sierant jaroslaw.sierant@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gtest/gtest.h"
#include <cstdint>
#include <limits>

#include "../SafeInt.hpp"
#include "CalculatorMock.hpp"

namespace
{
template < typename t_type, typename t_opType>
class OverflowHandlerTest
{
public:
   template <typename t_operationType>
   static void handleOverflow(t_type& p_result, const t_type p_cvalue, const EOverflowType p_type, const t_operationType p_value = 0) {
        ASSERT_EQ(currentValue, p_cvalue);
        ASSERT_EQ(operationValue, p_value);
        ASSERT_EQ(overflowType, p_type);
        p_result = result;
        handleOverflowCalls++;
   }

   static void handleDivByZero(t_type& p_result, const t_type p_value) {
      p_result = result;
      ASSERT_EQ(currentValue, p_value);
      handleDivByZeroCalls++;
   }
    static t_type currentValue;
    static t_opType operationValue;
    static EOverflowType overflowType;

    static t_type result;
    static unsigned int handleOverflowCalls;
    static unsigned int handleDivByZeroCalls;
private:
};

typedef OverflowHandlerTest<uint64_t, uint32_t> OverflowHandlerTestu64u32_t;
typedef OverflowHandlerTest<uint32_t, uint32_t> OverflowHandlerTestu32u32_t;
typedef OverflowHandlerTest<int64_t, int16_t> OverflowHandlerTesti64i16_t;

template<>
uint64_t OverflowHandlerTestu64u32_t::currentValue = 0;
template<>
uint32_t OverflowHandlerTestu64u32_t::operationValue = 0;
template<>
EOverflowType OverflowHandlerTestu64u32_t::overflowType = EOverflowType_negative;
template<>
unsigned int OverflowHandlerTestu64u32_t::handleOverflowCalls = 0;
template<>
unsigned int OverflowHandlerTestu64u32_t::handleDivByZeroCalls = 0;
template<>
uint64_t OverflowHandlerTestu64u32_t::result = 0;

template<>
uint32_t OverflowHandlerTestu32u32_t::currentValue = 0;
template<>
uint32_t OverflowHandlerTestu32u32_t::operationValue = 0;
template<>
EOverflowType OverflowHandlerTestu32u32_t::overflowType = EOverflowType_negative;
template<>
unsigned int OverflowHandlerTestu32u32_t::handleOverflowCalls = 0;
template<>
unsigned int OverflowHandlerTestu32u32_t::handleDivByZeroCalls = 0;
template<>
uint32_t OverflowHandlerTestu32u32_t::result = 0;

template<>
int64_t OverflowHandlerTesti64i16_t::currentValue = 0;
template<>
int16_t OverflowHandlerTesti64i16_t::operationValue = 0;
template<>
EOverflowType OverflowHandlerTesti64i16_t::overflowType = EOverflowType_negative;
template<>
unsigned int OverflowHandlerTesti64i16_t::handleOverflowCalls = 0;
template<>
unsigned int OverflowHandlerTesti64i16_t::handleDivByZeroCalls = 0;
template<>
int64_t OverflowHandlerTesti64i16_t::result = 0;

class SafeIntTest : public ::testing::Test
{
protected:
    SafeIntTest() { }
    virtual ~SafeIntTest() { }
    virtual void SetUp()
    {
      init();
    }
    virtual void TearDown() { }

   void init()
   {
        OverflowHandlerTestu64u32_t::handleOverflowCalls = 0;
        OverflowHandlerTestu32u32_t::handleOverflowCalls = 0;
        OverflowHandlerTesti64i16_t::handleOverflowCalls = 0;

        OverflowHandlerTestu64u32_t::handleDivByZeroCalls = 0;
        OverflowHandlerTesti64i16_t::handleDivByZeroCalls = 0;
        OverflowHandlerTestu32u32_t::handleDivByZeroCalls = 0;
   }
    template <typename t_mainType, typename t_multiType >
    void testMiltiplicationMaxOverflow(const t_mainType p_base = std::numeric_limits<t_mainType>::max()-std::numeric_limits<t_mainType>::max()/3,
                                       const t_multiType p_multip = 5)
    {
         init();
        SafeInt<t_mainType, OverflowHandlerTest<t_mainType, t_multiType> > l_testObject(p_base);
        SafeInt<t_mainType, OverflowHandlerTest<t_mainType, t_multiType> > l_afterOp;
        OverflowHandlerTest<t_mainType, t_multiType>::currentValue = p_base;
        OverflowHandlerTest<t_mainType, t_multiType>::operationValue = p_multip;
        OverflowHandlerTest<t_mainType, t_multiType>::overflowType = EOverflowType_positive;
        l_afterOp = l_testObject * p_multip;
        ASSERT_EQ(p_base, l_testObject.value());
        typedef OverflowHandlerTest<t_mainType, t_multiType> type_t;
        ASSERT_EQ(1U, type_t::handleOverflowCalls);
    }

    template <typename t_mainType, typename t_multiType >
    void testMiltiplication(const t_mainType p_base, const t_multiType p_multip)
    {
        init();
        t_mainType l_initValue = p_base;
        SafeInt<t_mainType> l_testObject(l_initValue);
        SafeInt<t_mainType> l_afterOp;
        ASSERT_NO_THROW(l_afterOp = l_testObject * p_multip);
        ASSERT_EQ(l_afterOp.value(), p_base*p_multip);
    }


    template <typename t_mainType, typename t_multiType >
    void testMiltiplicationOverflowMin(const t_mainType p_base, const t_multiType p_multip)
    {
       init();
        t_mainType l_initValue = p_base;
        SafeInt<t_mainType, OverflowHandlerTest<t_mainType, t_multiType> > l_testObject(l_initValue);
        SafeInt<t_mainType, OverflowHandlerTest<t_mainType, t_multiType> > l_afterOp;
        OverflowHandlerTest<t_mainType, t_multiType>::currentValue = l_initValue;
        OverflowHandlerTest<t_mainType, t_multiType>::operationValue = p_multip;
        OverflowHandlerTest<t_mainType, t_multiType>::overflowType = EOverflowType_negative;
        l_afterOp = l_testObject * p_multip;
        typedef OverflowHandlerTest<t_mainType, t_multiType> type_t;
        ASSERT_EQ(1U, type_t::handleOverflowCalls);
    }
    template <typename T1, typename T2>
    void testdivisionDivideByZero()
    {
      SafeInt<T1, OverflowHandlerTest<T1,T2> > l_int1(T2(5));
      SafeInt<T1, OverflowHandlerTest<T1,T2> > l_int2;
      OverflowHandlerTest<T1,T2>::currentValue = 5;
      OverflowHandlerTest<T1,T2>::result = 8;
      ASSERT_NO_THROW(l_int2 = l_int1/T2(0));
      ASSERT_EQ(l_int2.value(),(OverflowHandlerTest<T1,T2>::result));
      ASSERT_EQ(l_int1.value(), (OverflowHandlerTest<T1,T2>::currentValue));
      ASSERT_EQ(1, (OverflowHandlerTest<T1,T2>::handleDivByZeroCalls));
      ASSERT_EQ(0, (OverflowHandlerTest<T1,T2>::handleOverflowCalls));
      init();
      OverflowHandlerTest<T1,T2>::result = 10;
      SafeInt<T1, OverflowHandlerTest<T1,T2> > l_int3(T2(0));
//        l_int2 = l_int1/l_int3;
      ASSERT_EQ(l_int2.value(), (OverflowHandlerTest<T1,T2>::result));
      ASSERT_EQ(l_int1.value(), (OverflowHandlerTest<T1,T2>::currentValue));
      ASSERT_EQ(1, (OverflowHandlerTest<T1,T2>::handleDivByZeroCalls));
      ASSERT_EQ(0, (OverflowHandlerTest<T1,T2>::handleOverflowCalls));
    }
};



TEST_F(SafeIntTest, inRange)
{
   ASSERT_TRUE((InRange<uint32_t, uint32_t>::value));
   ASSERT_TRUE((InRange<uint32_t, uint16_t>::value));
   ASSERT_TRUE((InRange<uint32_t, uint8_t>::value));
   ASSERT_FALSE((InRange<uint32_t, uint64_t>::value));

   ASSERT_TRUE((InRange<int16_t, int16_t>::value));
   ASSERT_TRUE((InRange<int16_t, int8_t>::value));
   ASSERT_FALSE((InRange<int16_t, int32_t>::value));

   ASSERT_FALSE((InRange<int64_t, uint64_t>::value));
   ASSERT_TRUE((InRange<int64_t, uint32_t>::value));
   ASSERT_TRUE((InRange<int64_t, uint16_t>::value));

   ASSERT_FALSE((InRange<uint64_t, int64_t>::value));
   ASSERT_FALSE((InRange<uint64_t, int32_t>::value));
   ASSERT_FALSE((InRange<uint64_t, int16_t>::value));
   ASSERT_FALSE((InRange<uint64_t, int8_t>::value));

}

TEST_F(SafeIntTest, smallerRange)
{
   ASSERT_FALSE((SmallerRange<uint32_t, uint32_t>::value));
   ASSERT_TRUE((SmallerRange<uint32_t, uint16_t>::value));
   ASSERT_TRUE((SmallerRange<uint32_t, uint8_t>::value));
   ASSERT_FALSE((SmallerRange<uint32_t, uint64_t>::value));

   ASSERT_FALSE((SmallerRange<int16_t, int16_t>::value));
   ASSERT_TRUE((SmallerRange<int16_t, int8_t>::value));
   ASSERT_FALSE((SmallerRange<int16_t, int32_t>::value));

   ASSERT_FALSE((SmallerRange<int64_t, uint64_t>::value));
   ASSERT_TRUE((SmallerRange<int64_t, uint32_t>::value));
   ASSERT_TRUE((SmallerRange<int64_t, uint16_t>::value));

   ASSERT_FALSE((SmallerRange<uint64_t, int64_t>::value));
   ASSERT_FALSE((SmallerRange<uint64_t, int32_t>::value));
   ASSERT_FALSE((SmallerRange<uint64_t, int16_t>::value));
   ASSERT_FALSE((SmallerRange<uint64_t, int8_t>::value));

}

TEST_F(SafeIntTest, assignOperator)
{
    SafeInt<uint32_t> l_testObject;
    SafeInt<uint32_t> l_testObject2(uint32_t(50));
    SafeInt<uint8_t> l_testObject3(uint8_t(5));
    SafeInt<int64_t> l_testObject4(int32_t(-5));
    l_testObject = uint32_t(1150);
    ASSERT_EQ(1150, l_testObject.value());
    l_testObject = l_testObject2;
    ASSERT_EQ(50, l_testObject.value());
    l_testObject = l_testObject3;
    ASSERT_EQ(5, l_testObject.value());
    l_testObject4 = l_testObject3;
    ASSERT_EQ(5, l_testObject4.value());
    l_testObject4 = l_testObject2;
    ASSERT_EQ(50, l_testObject4.value());
}

TEST_F(SafeIntTest, multiplicationOverflowUnsigned)
{
    testMiltiplicationMaxOverflow<uint64_t,uint32_t>();
}
//
TEST_F(SafeIntTest, multiplicationOverflowSigned)
{
    testMiltiplicationMaxOverflow<int64_t,int16_t>();
}
//
TEST_F(SafeIntTest, multiplicationMinOverflowSigned)
{
    testMiltiplicationOverflowMin<int64_t,int16_t>(
        (std::numeric_limits<int64_t>::min()-std::numeric_limits<int64_t>::min()/3),
        5);
}
//
TEST_F(SafeIntTest, multiplicationMaxOverflowSignedMinusOne)
{
    testMiltiplicationMaxOverflow<int64_t,int16_t>(
        std::numeric_limits<int64_t>::min(),
        -1);
}


TEST_F(SafeIntTest, multiplicationU64U64)
{
    testMiltiplication<uint64_t,uint64_t>(2,9);
    testMiltiplication<uint64_t,uint64_t>(90,0);
    testMiltiplication<uint64_t,uint64_t>(0,0);
    testMiltiplication<uint64_t,uint64_t>(0,5);
}

TEST_F(SafeIntTest, multiplicationI32I16)
{
    testMiltiplication<int32_t,int16_t>(40,-9);
    testMiltiplication<int32_t,int16_t>(-40,9);
    testMiltiplication<int32_t,int16_t>(-40,0);
    testMiltiplication<int32_t,int16_t>(0,-40);
    testMiltiplication<int32_t,int16_t>(0,40);
}

TEST_F(SafeIntTest, multiplicationSafeInt)
{
    SafeInt<uint16_t> l_int1(uint16_t(5));
    SafeInt<uint8_t> l_int2(uint8_t(6));
    SafeInt<uint16_t> l_int3(uint16_t(8));
    ASSERT_NO_THROW(l_int3 = l_int1 * l_int2);
    ASSERT_EQ(5U*6U, l_int3.value());
    ASSERT_EQ(5U, l_int1.value());
    ASSERT_EQ(6U, l_int2.value());
    l_int2 = uint8_t(10);
    ASSERT_NO_THROW(l_int3 = l_int2 * l_int1);
    ASSERT_EQ(5U*10, l_int3.value());
    ASSERT_EQ(5U, l_int1.value());
    ASSERT_EQ(10, l_int2.value());

    l_int2 = uint8_t(7);
    ASSERT_NO_THROW(l_int3 = l_int2 * uint16_t(7));
    ASSERT_EQ(7*7, l_int3.value());
    ASSERT_EQ(7, l_int2.value());
}

TEST_F(SafeIntTest, multiplicationSafeIntDifferentTypes)
{
    SafeInt<uint16_t> l_int1(uint16_t(5U));
    SafeInt<uint8_t> l_int2(uint8_t(6U));
    SafeInt<uint16_t> l_int3(uint16_t(8U));
    ASSERT_NO_THROW(l_int3 = l_int1 * l_int2);
    ASSERT_EQ(5U*6U, l_int3.value());
    ASSERT_EQ(5U, l_int1.value());
    ASSERT_EQ(6U, l_int2.value());
    ASSERT_NO_THROW(l_int3 = l_int2 * l_int1);
    ASSERT_EQ(5U*6U, l_int3.value());
    ASSERT_EQ(5U, l_int1.value());
    ASSERT_EQ(6U, l_int2.value());
}

TEST_F(SafeIntTest, comparition)
{
    SafeInt<uint16_t> l_int1(uint16_t(5U));
    SafeInt<uint8_t> l_int2(uint8_t(5U));
    ASSERT_TRUE(l_int1 == l_int2);
    ASSERT_TRUE(l_int1 == uint16_t(5U));
    ASSERT_TRUE(l_int2 == uint8_t(5U));
    l_int1 = uint16_t(6U);
    l_int2 = uint8_t(9U);
    ASSERT_FALSE(l_int1 == l_int2);
    ASSERT_FALSE(l_int1 == uint16_t(5U));
    ASSERT_FALSE(l_int2 == uint8_t(5U));
}

TEST_F(SafeIntTest, constCheck)
{
    const SafeInt<uint16_t> l_int1(uint16_t(5));
    const SafeInt<uint8_t> l_int2(uint8_t(5));
    const SafeInt<uint16_t> l_int3 = l_int1;
    ASSERT_TRUE(l_int3 == l_int1);
    SafeInt<uint16_t> l_int4 = l_int1;
    ASSERT_TRUE(l_int4 == l_int1);
    SafeInt<uint16_t> l_int5 = l_int1 * uint8_t(5);
    ASSERT_TRUE(l_int5 == uint8_t(5*5));
    SafeInt<uint16_t> l_int6 = l_int1 * l_int2;
    ASSERT_TRUE(l_int6 == uint8_t(5*5));
}

TEST_F(SafeIntTest, modulo)
{
    SafeInt<int16_t> l_int1(int16_t(5));
    SafeInt<int8_t> l_int2(int8_t(2));
    ASSERT_TRUE(5%2 == (l_int1%l_int2).value());
    ASSERT_TRUE(5%2 == (l_int1%int16_t(2)).value());
}

TEST_F(SafeIntTest, operatorLessThan)
{
//     SafeInt<int16_t> l_int1(int16_t(5));
//     SafeInt<int8_t> l_int2(int8_t(2));
//     ASSERT_TRUE(l_int2 < l_int1);
//     ASSERT_FALSE(l_int1 < l_int2);
//     ASSERT_TRUE(l_int2 < int8_t(8));
}

TEST_F(SafeIntTest, increment)
{
//     SafeInt<int16_t> l_int1(int16_t(5));
//     ASSERT_TRUE(l_int1.value() == int16_t(5));
//     ASSERT_TRUE(++l_int1 == int16_t(6));
//     SafeInt<uint32_t, OverflowHandlerTest<uint32_t, uint32_t> > l_int2(uint32_t(std::numeric_limits<uint32_t>::max()));
//     OverflowHandlerTest<uint32_t, uint32_t>::currentValue = l_int2.value();
//     OverflowHandlerTest<uint32_t, uint32_t>::operationValue = 0;
//     OverflowHandlerTest<uint32_t, uint32_t>::overflowType = EOverflowType_positive;
//     OverflowHandlerTest<uint32_t, uint32_t>::result = 10;
//     ASSERT_NO_THROW(++l_int2);
//     ASSERT_EQ(1, (OverflowHandlerTest<uint32_t, uint32_t>::handleOverflowCalls));
//     ASSERT_EQ(0, (OverflowHandlerTest<uint32_t, uint32_t>::handleDivByZeroCalls));
//     ASSERT_EQ(10, l_int2.value());
}
TEST_F(SafeIntTest, decrement)
{
//     SafeInt<int16_t> l_int1(int16_t(5));
//     ASSERT_TRUE(l_int1.value() == int16_t(5));
//     ASSERT_TRUE(--l_int1 == int16_t(4));
//     SafeInt<uint32_t, OverflowHandlerTest<uint32_t, uint32_t> > l_int2(uint32_t(0));
//     OverflowHandlerTest<uint32_t, uint32_t>::currentValue = l_int2.value();
//     OverflowHandlerTest<uint32_t, uint32_t>::operationValue = 0;
//     OverflowHandlerTest<uint32_t, uint32_t>::overflowType = EOverflowType_negative;
//     OverflowHandlerTest<uint32_t, uint32_t>::result = 10;
//     ASSERT_NO_THROW(--l_int2);
//     ASSERT_EQ(1, (OverflowHandlerTest<uint32_t, uint32_t>::handleOverflowCalls));
//     ASSERT_EQ(0, (OverflowHandlerTest<uint32_t, uint32_t>::handleDivByZeroCalls));
//     ASSERT_EQ(10, l_int2.value());
}


TEST_F(SafeIntTest, division)
{
//     SafeInt<int16_t> l_int1(int16_t(5));
//     SafeInt<int16_t> l_int2(int16_t(2));
//     ASSERT_EQ(int16_t(5/2), l_int1/int16_t(2));
//     ASSERT_EQ(int16_t(5/2), l_int1/l_int2);
}


TEST_F(SafeIntTest, divisionDivideByZero)
{
//     testdivisionDivideByZero<uint64_t,uint32_t>();
//     testdivisionDivideByZero<int64_t,int16_t>();
//     SafeInt<uint64_t, OverflowHandlerTestu64u32_t> l_int1(uint16_t(5));
//     SafeInt<uint64_t, OverflowHandlerTestu64u32_t> l_int2;
//     OverflowHandlerTestu64u32_t::currentValue = 5;
//     OverflowHandlerTestu64u32_t::result = 8;
//     ASSERT_NO_THROW(l_int2 = l_int1/uint16_t(0));
//     ASSERT_EQ(l_int2.value(), OverflowHandlerTestu64u32_t::result);
//     ASSERT_EQ(l_int1.value(), OverflowHandlerTestu64u32_t::currentValue);
//     ASSERT_EQ(1, OverflowHandlerTestu64u32_t::handleDivByZeroCalls);
//     ASSERT_EQ(0, OverflowHandlerTestu64u32_t::handleOverflowCalls);
}

TEST_F(SafeIntTest, divisionMinDividedByMinusOne)
{
//     SafeInt<int64_t, OverflowHandlerTesti64i16_t> l_int1(std::numeric_limits<int64_t>::min());
//     SafeInt<int64_t, OverflowHandlerTesti64i16_t> l_int2;
//     OverflowHandlerTesti64i16_t::currentValue = std::numeric_limits<int64_t>::min();
//     OverflowHandlerTesti64i16_t::result = 8;
//     ASSERT_NO_THROW(l_int2 = l_int1/int16_t(-1));
//     ASSERT_EQ(l_int2.value(), OverflowHandlerTesti64i16_t::result);
//     ASSERT_EQ(l_int1.value(), OverflowHandlerTesti64i16_t::currentValue);
//     ASSERT_EQ(l_int1.value(), OverflowHandlerTesti64i16_t::currentValue);
//
//     ASSERT_EQ(1, OverflowHandlerTesti64i16_t::handleOverflowCalls);
//     ASSERT_EQ(0, OverflowHandlerTesti64i16_t::handleDivByZeroCalls);
}

} // namespace

// kate: indent-mode cstyle; replace-tabs on;
