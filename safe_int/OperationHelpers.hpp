/*
    Safe Integer library
    Copyright (C) 2011  Jarosław Sierant jaroslaw.sierant@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SAFEINTHELPERS_HPP
#define SAFEINTHELPERS_HPP

#include <limits>
#include <boost/type_traits.hpp>
#include "EOverflowType.h"
#include "force_inline.hpp"

namespace safe_int
{

namespace
{
template <typename T1, typename T2>
inline bool isSignedIntMinusOneProblem(const T1 p_lhs, const T2 p_rhs)
{
	return p_lhs == std::numeric_limits<T1>::min() && p_rhs == -1;
}

}

template < typename t_lhs,
         typename t_rhs,
         typename t_exceptionHandler,
         bool isSigned = boost::is_signed<t_lhs>::value >
class MultiplicationHelper
{
public:
	static force_inline void multiply(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result);
};

template < typename t_lhs,
         typename t_rhs,
         typename t_exceptionHandler,
         bool isSigned = boost::is_signed<t_lhs>::value >
class DivisionHelper
{
public:
	static force_inline void divide(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result);
};

template < typename t_lhs,
         typename t_rhs,
         typename t_exceptionHandler,
         bool isSigned = boost::is_signed<t_lhs>::value >
class SubtractionHelper
{
public:
	static force_inline void subtract(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result);
};

template < typename t_lhs,
         typename t_rhs,
         typename t_exceptionHandler,
         bool isSigned = boost::is_signed<t_lhs>::value >
class AdditionHelper
{
public:
	static force_inline void add(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result);
};


template < typename t_lhs,
         typename t_rhs,
         typename t_exceptionHandler >
class MultiplicationHelper<t_lhs, t_rhs, t_exceptionHandler, false>
{
public:
	static force_inline void multiply(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result) {
		p_result = p_lhs * p_rhs;
		if(p_result != 0 && p_result / p_lhs != p_rhs) {
			t_exceptionHandler::handleOverflow(p_result, EOverflowType_positive);
		}
	}
};

template < typename t_lhs,
         typename t_rhs,
         typename t_exceptionHandler >
class MultiplicationHelper<t_lhs, t_rhs, t_exceptionHandler, true>
{
public:
	static force_inline void multiply(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result) {
		if(isSignedIntMinusOneProblem(p_lhs, p_rhs) || ((p_result = p_lhs * p_rhs) != 0 && (p_result / p_lhs != p_rhs))) {
			t_exceptionHandler::handleOverflow(p_result,
															  ((p_lhs ^ p_rhs) < 0) ? EOverflowType_negative : EOverflowType_positive);
		}
	}
};

template < typename t_lhs,
         typename t_rhs,
         typename t_exceptionHandler >
class DivisionHelper<t_lhs, t_rhs, t_exceptionHandler, false>
{
public:
	static force_inline void divide(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result) {
		if(p_rhs == 0)
			{ t_exceptionHandler::handleDivByZero(p_result, EOverflowType_positive); return; }
		p_result = p_lhs / p_rhs;
	}
};

template < typename t_lhs,
         typename t_rhs,
         typename t_exceptionHandler >
class DivisionHelper<t_lhs, t_rhs, t_exceptionHandler, true>
{
public:
	static force_inline void divide(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result) {
		if(p_rhs == 0)
			{ t_exceptionHandler::handleDivByZero(p_result, (p_lhs < 0) ? EOverflowType_negative : EOverflowType_positive); return; }

		if(isSignedIntMinusOneProblem(p_lhs, p_rhs)) {
			t_exceptionHandler::handleOverflow(p_result, EOverflowType_positive);
			return;
		}
		p_result = p_lhs / p_rhs;
	}
};


template < typename t_lhs,
         typename t_rhs,
         typename t_exceptionHandler >
class SubtractionHelper<t_lhs, t_rhs, t_exceptionHandler, false>
{
public:
	static force_inline void subtract(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result) {
		if(p_rhs > p_lhs) {
			t_exceptionHandler::handleOverflow(p_result, EOverflowType_negative);
			return;
		}
		p_result = p_lhs - p_rhs;
	}
};


template < typename t_lhs,
         typename t_rhs,
         typename t_exceptionHandler >
class SubtractionHelper<t_lhs, t_rhs, t_exceptionHandler, true>
{
public:
	static force_inline void subtract(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result) {
		if(p_rhs < 0)
			{ AdditionHelper<t_lhs, t_rhs, t_exceptionHandler>::add(p_lhs, p_rhs, p_result); return; }
		if(std::numeric_limits<t_lhs>::min() + p_rhs > p_lhs) {
			t_exceptionHandler::handleOverflow(p_result, EOverflowType_negative);
			return;
		}
		p_result = p_lhs - p_rhs;
	}
};


template < typename t_lhs,
         typename t_rhs,
         typename t_exceptionHandler >
class AdditionHelper<t_lhs, t_rhs, t_exceptionHandler, false>
{
public:
	static force_inline void add(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result) {
		if(std::numeric_limits<t_lhs>::max() - p_lhs < p_rhs) {
			t_exceptionHandler::handleOverflow(p_result, EOverflowType_positive);
			return;
		}
		p_result = p_lhs + p_rhs;
	}
};

template < typename t_lhs,
         typename t_rhs,
         typename t_exceptionHandler >
class AdditionHelper<t_lhs, t_rhs, t_exceptionHandler, true>
{
public:
	static force_inline void add(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result) {
		if(p_rhs < 0)
			{ SubtractionHelper<t_lhs, t_rhs, t_exceptionHandler>::subtract(p_lhs, p_rhs, p_result); return; }
		if(std::numeric_limits<t_lhs>::max() - p_rhs < p_lhs) {
			t_exceptionHandler::handleOverflow(p_result, EOverflowType_positive);
			return;
		}
		p_result = p_lhs + p_rhs;
	}
};

} // namespace safe_int

#endif // SAFEINTHELPERS_HPP
// kate: indent-mode cstyle; indent-width 3; replace-tabs off; tab-width 3;
