/*
    Safe Integer library
    Copyright (C) 2011  Jarosław Sierant jaroslaw.sierant@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SAFEINTEXCEPTIONDEFAULTHANDLER_HPP
#define SAFEINTEXCEPTIONDEFAULTHANDLER_HPP
#include <exception>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits.hpp>
#include "EOverflowType.h"

namespace safe_int
{

class Exception : public std::exception { };

template <typename t_exception = Exception >
class ExceptionDefaultHandler
{
public:
	template <typename t_type>
	static void handleOverflow(t_type&, const EOverflowType) {
		throw t_exception();
	}

	template <typename t_type>
	static void handleDivByZero(t_type&, const EOverflowType) {
		throw t_exception();
	}
};

} //namespace safe_int

#endif // SAFEINTEXCEPTIONDEFAULTHANDLER_HPP
// kate: indent-mode cstyle; indent-width 3; replace-tabs off; tab-width 3;
