/*
    Safe Integer library
    Copyright (C) 2011  Jarosław Sierant jaroslaw.sierant@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RANGE_HPP
#define RANGE_HPP

#include <boost/type_traits.hpp>

namespace safe_int
{

template <typename t_type, typename t_typeToCheck>
struct InRange {
	static const bool value = ((sizeof(t_type) >= sizeof(t_typeToCheck)) &&
	      (boost::is_signed<t_type>::value == boost::is_signed<t_typeToCheck>::value)) ||
	      (boost::is_signed<t_type>::value &&
	       boost::is_unsigned<t_typeToCheck>::value &&
	       sizeof(t_type) >  sizeof(t_typeToCheck));
};

template <typename t_type, typename t_typeToCheck>
struct SmallerRange {
	static const bool value =((sizeof(t_type) > sizeof(t_typeToCheck)) &&
									   boost::is_signed<t_type>::value ==
									   boost::is_signed<t_typeToCheck>::value) ||
									  (boost::is_signed<t_type>::value &&
										boost::is_unsigned<t_typeToCheck>::value &&
									   sizeof(t_type) >  sizeof(t_typeToCheck));

};
} // namespace safe_int


#endif //RANGE_HPP
