/*
    Safe Integer library
    Copyright (C) 2011  Jarosław Sierant jaroslaw.sierant@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SAFE_INT_ENABLE_IF_HP
#define SAFE_INT_ENABLE_IF_HP

#include <boost/type_traits.hpp>
#include <boost/utility/enable_if.hpp>

#include "Range.hpp"

namespace safe_int
{

template <typename t_type, typename t_typeToCheck, typename t_returnType = void>
struct EnableIfInRange: public boost::enable_if_c< InRange<t_type, t_typeToCheck>::value, t_returnType> { };

template<typename t1, typename t2, typename r = void>
class EnableIfSameSign : public boost::enable_if_c<boost::is_signed<t1>::value == boost::is_signed<t2>::value, r> {};

template<typename t_type, typename t_typeToCheck, typename r = void>
class EnableIfSmallerRange : public boost::enable_if_c<SmallerRange<t_type, t_typeToCheck>::value, r> {};

} // namespace safe_int

#endif //SAFE_INT_ENABLE_IF_HP
