/*
    Safe Integer library
    Copyright (C) 2011  Jarosław Sierant jaroslaw.sierant@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"

#include "../safe_int/Range.hpp"

namespace safe_int
{
namespace ut
{
class RangeTest : public ::testing::Test
{
protected:
	RangeTest() { }
	virtual ~RangeTest() { }
	virtual void SetUp() { }
	virtual void TearDown() { }
};


TEST_F(RangeTest, inRangeUnsigned)
{
   ASSERT_TRUE((InRange<uint32_t, uint32_t>::value));
   ASSERT_TRUE((InRange<uint32_t, uint16_t>::value));
   ASSERT_TRUE((InRange<uint32_t, uint8_t>::value));
   ASSERT_FALSE((InRange<uint32_t, uint64_t>::value));
}

TEST_F(RangeTest, inRangeSigned)
{
   ASSERT_TRUE((InRange<int16_t, int16_t>::value));
   ASSERT_TRUE((InRange<int16_t, int8_t>::value));
   ASSERT_FALSE((InRange<int16_t, int32_t>::value));
}

TEST_F(RangeTest, inRangeSignedUnsigned)
{
   ASSERT_FALSE((InRange<int64_t, uint64_t>::value));
   ASSERT_TRUE((InRange<int64_t, uint32_t>::value));
   ASSERT_TRUE((InRange<int64_t, uint16_t>::value));
}

TEST_F(RangeTest, inRangeUnsignedSigned)
{
   ASSERT_FALSE((InRange<uint64_t, int64_t>::value));
   ASSERT_FALSE((InRange<uint64_t, int32_t>::value));
   ASSERT_FALSE((InRange<uint64_t, int16_t>::value));
   ASSERT_FALSE((InRange<uint64_t, int8_t>::value));
}

TEST_F(RangeTest, smallerRangeUnsigned)
{
   ASSERT_FALSE((SmallerRange<uint32_t, uint32_t>::value));
   ASSERT_TRUE((SmallerRange<uint32_t, uint16_t>::value));
   ASSERT_TRUE((SmallerRange<uint32_t, uint8_t>::value));
   ASSERT_FALSE((SmallerRange<uint32_t, uint64_t>::value));
}


TEST_F(RangeTest, smallerRangeSigned)
{
   ASSERT_FALSE((SmallerRange<int16_t, int16_t>::value));
   ASSERT_TRUE((SmallerRange<int16_t, int8_t>::value));
   ASSERT_FALSE((SmallerRange<int16_t, int32_t>::value));
}

TEST_F(RangeTest, smallerRangeSignedUnsigned)
{
   ASSERT_FALSE((SmallerRange<int64_t, uint64_t>::value));
   ASSERT_TRUE((SmallerRange<int64_t, uint32_t>::value));
   ASSERT_TRUE((SmallerRange<int64_t, uint16_t>::value));

}

TEST_F(RangeTest, smallerRangeUnsignedSigned)
{
   ASSERT_FALSE((SmallerRange<uint64_t, int64_t>::value));
   ASSERT_FALSE((SmallerRange<uint64_t, int32_t>::value));
   ASSERT_FALSE((SmallerRange<uint64_t, int16_t>::value));
   ASSERT_FALSE((SmallerRange<uint64_t, int8_t>::value));

}

} // namespace ut
} // namespace safe_int

// kate: indent-mode cstyle; indent-width 3; replace-tabs off; tab-width 3;
