/*
    Safe Integer library
    Copyright (C) 2011  Jarosław Sierant jaroslaw.sierant@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SAFE_INT_CALCULATOR_HPP
#define SAFE_INT_CALCULATOR_HPP

#include <limits>

#include "OperationHelpers.hpp"

namespace safe_int
{


template <typename t_exceptionHandler>
class Calculator
{
public:
	template <typename t_lhs, typename t_rhs>
	static force_inline void add(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result)
	{
		AdditionHelper<t_lhs, t_rhs, t_exceptionHandler>::add(p_lhs, p_rhs, p_result);
	}
	template <typename t_lhs, typename t_rhs>
	static force_inline void subtract(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result)
	{
		SubtractionHelper<t_lhs, t_rhs, t_exceptionHandler>::subtract(p_lhs, p_rhs, p_result);
	}
	template <typename t_lhs, typename t_rhs>
	static force_inline void multiply(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result)
	{
		MultiplicationHelper<t_lhs, t_rhs, t_exceptionHandler>::multiply(p_lhs, p_rhs, p_result);
	}
	template <typename t_lhs, typename t_rhs>
	static force_inline void divide(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result)
	{
		DivisionHelper<t_lhs, t_rhs, t_exceptionHandler>::divide(p_lhs, p_rhs, p_result);
	}
	template <typename t_lhs>
	static force_inline void inc(const t_lhs p_lhs, t_lhs& p_result)
	{
		if (p_lhs == std::numeric_limits<t_lhs>::max())
		{
			t_exceptionHandler::handleOverflow(p_result, EOverflowType_positive);
			return;
		}
		++p_result;
	}
	template <typename t_lhs>
	static force_inline void dec(const t_lhs p_lhs, t_lhs& p_result)
	{
		if (p_lhs == std::numeric_limits<t_lhs>::min())
		{
			t_exceptionHandler::handleOverflow(p_result, EOverflowType_negative);
			return;
		}
		--p_result;
	}
};

} //namespace safe_int

#endif // SAFE_INT_CALCULATOR_HPP
