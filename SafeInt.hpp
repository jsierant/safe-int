/*
    Safe Integer library
    Copyright (C) 2011  Jarosław Sierant jaroslaw.sierant@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SAFEINT_HPP
#define SAFEINT_HPP

#include "safe_int/ExceptionDefaultHandler.hpp"
#include "safe_int/Calculator.hpp"
#include "safe_int/EnableIf.hpp"

template < typename t_type,
			  typename t_exceptionHandler = safe_int::ExceptionDefaultHandler<>,
			  typename t_calculator = safe_int::Calculator<t_exceptionHandler>
         >
class SafeInt
{
public:
	BOOST_STATIC_ASSERT((boost::is_integral<t_type>::value && !boost::is_same<t_type, bool>::value));
	SafeInt() throw() : m_value(0) { }
	template <typename t_assignType>
	explicit SafeInt(const t_assignType p_value, typename safe_int::EnableIfInRange<t_type, t_assignType>::type* = 0) throw()
		: m_value(p_value)
	{ }

// 	template <typename t_assignType>
// 	explicit SafeInt(const SafeInt<t_assignType, t_exceptionHandler, t_calculator>& p_value,
// 						  typename safe_int::EnableIfInRange<t_type, t_assignType>::type* = 0) throw()
// 		: m_value(p_value.value())
// 	{ }

	template <typename t_assignType>
	inline typename safe_int::EnableIfInRange<t_type, t_assignType, SafeInt<t_type, t_exceptionHandler, t_calculator>&>::type
	operator=(const t_assignType p_value) throw() {
		m_value = p_value;
		return *this;
	}
	template <typename t_assignType>
	inline typename safe_int::EnableIfInRange<t_type, t_assignType, SafeInt<t_type, t_exceptionHandler, t_calculator> &>::type
	operator=(const SafeInt<t_assignType, t_exceptionHandler, t_calculator>& p_value) throw() {
		m_value = p_value.value();
		return *this;
	}

	template <typename t_opr>
	typename safe_int::EnableIfInRange<t_type, t_opr, SafeInt< t_type, t_exceptionHandler, t_calculator> >::type
	operator*(const t_opr p_value) const {
		t_type l_result;
		t_calculator::template multiply<t_type, t_opr>(m_value, p_value, l_result);
		return SafeInt<t_type, t_exceptionHandler, t_calculator>(l_result);
	}
	template <typename t_opr>
	typename safe_int::EnableIfInRange<t_type, t_opr, const SafeInt<t_type, t_exceptionHandler, t_calculator> >::type
	operator*(const SafeInt<t_opr, t_exceptionHandler, t_calculator>& p_value) const {
		return (*this) * p_value.value();
	}
	template <typename t_opr>
	typename safe_int::EnableIfSmallerRange<t_opr, t_type, SafeInt<t_opr, t_exceptionHandler, t_calculator> >::type
	operator*(const t_opr p_value) const {
		return SafeInt<t_opr, t_exceptionHandler, t_calculator>(m_value)*p_value;
	}
	template <typename t_opr>
	typename safe_int::EnableIfSmallerRange<t_opr, t_type, SafeInt<t_opr, t_exceptionHandler, t_calculator> >::type
	operator*(const SafeInt<t_opr, t_exceptionHandler, t_calculator>& p_value) const {
		return p_value * m_value;
	}


	template <typename t_opr>
	typename safe_int::EnableIfSameSign<t_type, t_opr, const SafeInt<t_type, t_exceptionHandler, t_calculator> >::type
	operator/(const t_opr p_value) const {
		t_type l_result;
		t_calculator::template divide<t_type, t_opr>(m_value, p_value, l_result);
		return SafeInt<t_type, t_exceptionHandler, t_calculator>(l_result);
	}
	template <typename t_opr>
	typename safe_int::EnableIfSameSign<t_type, t_opr, const SafeInt<t_type, t_exceptionHandler, t_calculator> >::type
	operator/(const SafeInt<t_opr, t_exceptionHandler, t_calculator>& p_value) const {
		return (*this)/p_value.value();
	}

	template <typename t_opr>
	inline typename safe_int::EnableIfInRange<t_type, t_opr, const SafeInt<t_type, t_exceptionHandler, t_calculator> >::type
	operator+(const t_opr p_value) {
		return SafeInt<t_type, t_exceptionHandler, t_calculator>(*this) += p_value;
	}
	template <typename t_opr>
	inline typename safe_int::EnableIfSmallerRange<t_opr, t_type, const SafeInt<t_opr, t_exceptionHandler, t_calculator> >::type
	operator+(const t_opr p_value) {
		return SafeInt<t_opr, t_exceptionHandler, t_calculator>(m_value) += p_value;
	}

	template <typename t_opr>
	typename safe_int::EnableIfSmallerRange<t_opr, t_type, const SafeInt<t_opr, t_exceptionHandler, t_calculator> >::type
	operator+(SafeInt<t_opr, t_exceptionHandler, t_calculator> p_value) {
		return p_value += m_value;
	}

	template <typename t_opr>
	typename safe_int::EnableIfInRange<t_type, t_opr, SafeInt<t_type, t_exceptionHandler, t_calculator> &>::type
	operator+=(const t_opr p_value) {
		t_calculator::template add<t_type, t_opr>(m_value, p_value, m_value);
		return *this;
	}
//
	template <typename t_opr>
	typename safe_int::EnableIfInRange<t_type, t_opr, SafeInt<t_type, t_exceptionHandler, t_calculator>&>::type
	operator+=(const SafeInt<t_opr, t_exceptionHandler, t_calculator>& p_value) {
		return (*this) += p_value.value();
	}
// //
	template <typename t_opr> inline
	typename safe_int::EnableIfSameSign<t_type, t_opr, bool>::type
	operator==(const t_opr p_value) const throw() { return m_value == p_value; }
	template <typename t_opr> inline
	typename safe_int::EnableIfSameSign<t_type, t_opr, bool>::type
	operator==(const SafeInt<t_opr, t_exceptionHandler, t_calculator>& p_value) const throw() { return m_value == p_value.value(); }
	template <typename t_opr> inline
	typename safe_int::EnableIfSameSign<t_type, t_opr, bool>::type
	operator<(const t_opr p_value) const throw() { return m_value < p_value; }
	template <typename t_opr> inline
	typename safe_int::EnableIfSameSign<t_type, t_opr, bool>::type
	operator<(const SafeInt<t_opr, t_exceptionHandler, t_calculator>& p_value) const throw() { return m_value < p_value.value(); }

	template <typename t_opr>
	inline typename safe_int::EnableIfSameSign<t_type, t_opr, const SafeInt<t_type, t_exceptionHandler> >::type
	operator%(const t_opr p_value) const throw() {
		const t_type l_value=m_value%p_value;
		return SafeInt<t_type, t_exceptionHandler>(l_value);
	}
	template <typename t_opr>
	inline typename safe_int::EnableIfSameSign<t_type, t_opr, const SafeInt<t_type, t_exceptionHandler, t_calculator> >::type
	operator%(const SafeInt<t_opr, t_exceptionHandler, t_calculator>& p_value) const throw() {
		const t_type l_value=m_value%p_value.value();
		return SafeInt<t_type, t_exceptionHandler, t_calculator>(l_value);
	}

	SafeInt<t_type, t_exceptionHandler, t_calculator>& operator++()
	{
		t_calculator::template inc<t_type>(m_value, m_value);
		return *this;
	}
	SafeInt<t_type, t_exceptionHandler, t_calculator>& operator--()
	{
		t_calculator::template dec<t_type>(m_value, m_value);
		return *this;
	}

	t_type value() const { return m_value; }
private:
	t_type m_value;
};

#endif // SAFEINT_HPP
// kate: indent-mode cstyle; indent-width 3; replace-tabs off; tab-width 3;
