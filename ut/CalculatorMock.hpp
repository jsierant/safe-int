/*
    Safe Integer library
    Copyright (C) 2011  Jarosław Sierant jaroslaw.sierant@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CALCULATORMOCK_HPP
#define CALCULATORMOCK_HPP
namespace safe_int
{
namespace ut
{

template <typename t_exceptionHandler>
class CalculatorMock
{
public:
	template <typename t_lhs, typename t_rhs>
	static void add(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result)
	{
		s_lArg = p_lhs; s_rArg = p_rhs; p_result = s_result;
		++s_addCalls;
	}
	template <typename t_lhs, typename t_rhs>
	static void subtract(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result)
	{
		s_lArg = p_lhs; s_rArg = p_rhs; p_result = s_result;
		++s_subtractCalls;
	}
	template <typename t_lhs, typename t_rhs>
	static void multiply(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result)
	{
		s_lArg = p_lhs; s_rArg = p_rhs; p_result = s_result;
		++s_multiplyCalls;
	}
	template <typename t_lhs, typename t_rhs>
	static void divide(const t_lhs p_lhs, const t_rhs p_rhs, t_lhs& p_result)
	{
		s_lArg = p_lhs; s_rArg = p_rhs; p_result = s_result;
		++s_divideCalls;
	}
	template <typename t_lhs>
	static void inc(const t_lhs p_lhs, t_lhs& p_result)
	{
		s_lArg = p_lhs; p_result = s_result;
		++s_incCalls;
	}
	template <typename t_lhs>
	static void dec(const t_lhs p_lhs, t_lhs& p_result)
	{
		s_lArg = p_lhs; p_result = s_result;
		++s_decCalls;
	}
	static void reset()
	{
		s_addCalls = s_subtractCalls = s_multiplyCalls = s_divideCalls = s_incCalls = s_decCalls = 0;
		s_lArg = s_rArg = s_result = 0;

	}
	static uint32_t s_addCalls;
	static uint32_t s_subtractCalls;
	static uint32_t s_multiplyCalls;
	static uint32_t s_divideCalls;
	static uint32_t s_incCalls;
	static uint32_t s_decCalls;
	static int64_t s_lArg;
	static int64_t s_rArg;
	static int64_t s_result;
};

template <typename t_exceptionHandler> uint32_t CalculatorMock<t_exceptionHandler>::s_addCalls = 0;
template <typename t_exceptionHandler> uint32_t CalculatorMock<t_exceptionHandler>::s_subtractCalls = 0;
template <typename t_exceptionHandler> uint32_t CalculatorMock<t_exceptionHandler>::s_multiplyCalls = 0;
template <typename t_exceptionHandler> uint32_t CalculatorMock<t_exceptionHandler>::s_divideCalls = 0;
template <typename t_exceptionHandler> uint32_t CalculatorMock<t_exceptionHandler>::s_incCalls = 0;
template <typename t_exceptionHandler> uint32_t CalculatorMock<t_exceptionHandler>::s_decCalls = 0;

template <typename t_exceptionHandler> int64_t CalculatorMock<t_exceptionHandler>::s_lArg = 0;
template <typename t_exceptionHandler> int64_t CalculatorMock<t_exceptionHandler>::s_rArg = 0;
template <typename t_exceptionHandler> int64_t CalculatorMock<t_exceptionHandler>::s_result = 0;

} // namespace ut
} // namespace safe_int

#endif // CALCULATORMOCK_HPP
