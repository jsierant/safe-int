/*
    Safe Integer library
    Copyright (C) 2011  Jarosław Sierant jaroslaw.sierant@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef EXCEPTIONHANDLERMOCK_HPP
#define EXCEPTIONHANDLERMOCK_HPP

#include "../safe_int/EOverflowType.h"

namespace safe_int
{
namespace ut
{

class ExceptionHandlerMock
{
public:
		template <typename t_type>
	static void handleOverflow(t_type& p_result, const EOverflowType p_type) {
        p_result = s_result;
        s_handleOverflowCalls++;
		  s_overflowType = p_type;
	}

	template <typename t_type>
	static void handleDivByZero(t_type& p_result, const EOverflowType p_type) {
		p_result = s_result;
      s_handleDivByZeroCalls++;
		s_overflowType = p_type;
	}
   static void reset()
	{
		s_result = 0;
		s_overflowType = EOverflowType_negative;
		s_handleDivByZeroCalls = s_handleOverflowCalls = 0;
	}
    static EOverflowType s_overflowType;
    static int64_t s_result;
    static unsigned int s_handleOverflowCalls;
    static unsigned int s_handleDivByZeroCalls;
};
EOverflowType ExceptionHandlerMock::s_overflowType = EOverflowType_negative;
int64_t ExceptionHandlerMock::s_result = 0;
unsigned int ExceptionHandlerMock::s_handleOverflowCalls = 0;
unsigned int ExceptionHandlerMock::s_handleDivByZeroCalls = 0;

} // namespace ut
} // namespace safe_int

#endif // EXCEPTIONHANDLERMOCK_HPP
