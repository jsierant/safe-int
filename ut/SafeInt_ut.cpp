/*
    Safe Integer library
    Copyright (C) 2011  Jarosław Sierant jaroslaw.sierant@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"

#include "../SafeInt.hpp"
#include "CalculatorMock.hpp"
#include "ExceptionHandlerMock.hpp"

namespace safe_int
{
namespace ut
{

class SafeIntTest : public ::testing::Test
{
public:
	typedef SafeInt<uint32_t,
						 ExceptionHandlerMock,
						 CalculatorMock<ExceptionHandlerMock> >
			  SafeIntTestu32_t;
	typedef SafeInt<uint16_t,
						 ExceptionHandlerMock,
						 CalculatorMock<ExceptionHandlerMock> >
			  SafeIntTestu16_t;
protected:
   SafeIntTest() { }
   virtual ~SafeIntTest() { }
   virtual void SetUp()
	{
		ExceptionHandlerMock::reset();
		CalculatorMock<ExceptionHandlerMock>::reset();
	}
   virtual void TearDown() { }
   template<typename T> void multiplicationCheck(T p_lArg, T p_result)
	{
		ASSERT_EQ(0, (ExceptionHandlerMock::s_handleDivByZeroCalls));
		ASSERT_EQ(0, (ExceptionHandlerMock::s_handleOverflowCalls));
		ASSERT_EQ(1, (CalculatorMock<ExceptionHandlerMock>::s_multiplyCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_addCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_decCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_divideCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_incCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_subtractCalls));
		ASSERT_EQ(p_lArg, (CalculatorMock<ExceptionHandlerMock>::s_lArg));
		ASSERT_EQ((CalculatorMock<ExceptionHandlerMock>::s_result), p_result);
	}

	template<typename T> void divisionCheck(T p_lArg, T p_result)
	{
		ASSERT_EQ(0, (ExceptionHandlerMock::s_handleDivByZeroCalls));
		ASSERT_EQ(0, (ExceptionHandlerMock::s_handleOverflowCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_multiplyCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_addCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_decCalls));
		ASSERT_EQ(1, (CalculatorMock<ExceptionHandlerMock>::s_divideCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_incCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_subtractCalls));
		ASSERT_EQ(p_lArg, (CalculatorMock<ExceptionHandlerMock>::s_lArg));
		ASSERT_EQ((CalculatorMock<ExceptionHandlerMock>::s_result), p_result);
	}
	template<typename T> void additionCheck(T p_lArg, T p_result)
	{
		ASSERT_EQ(0, (ExceptionHandlerMock::s_handleDivByZeroCalls));
		ASSERT_EQ(0, (ExceptionHandlerMock::s_handleOverflowCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_multiplyCalls));
		ASSERT_EQ(1, (CalculatorMock<ExceptionHandlerMock>::s_addCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_decCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_divideCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_incCalls));
		ASSERT_EQ(0, (CalculatorMock<ExceptionHandlerMock>::s_subtractCalls));
		ASSERT_EQ(p_lArg, (CalculatorMock<ExceptionHandlerMock>::s_lArg));
		ASSERT_EQ((CalculatorMock<ExceptionHandlerMock>::s_result), p_result);
	}
};

TEST_F(SafeIntTest, assignOperator)
{
    SafeInt<uint32_t> l_testObject;
    SafeInt<uint32_t> l_testObject2(uint32_t(50));
    SafeInt<uint8_t> l_testObject3(uint8_t(5));
    SafeInt<int64_t> l_testObject4(int32_t(-5));
    l_testObject = uint32_t(1150);
    ASSERT_EQ(1150, l_testObject.value());
    l_testObject = l_testObject2;
    ASSERT_EQ(50, l_testObject.value());
    l_testObject = l_testObject3;
    ASSERT_EQ(5, l_testObject.value());
    l_testObject4 = l_testObject3;
    ASSERT_EQ(5, l_testObject4.value());
    l_testObject4 = l_testObject2;
    ASSERT_EQ(50, l_testObject4.value());
}

TEST_F(SafeIntTest, multiplicationSimpleType)
{
	SafeIntTestu32_t l_int;
   SafeIntTestu32_t l_testObject2(uint32_t(50));
	CalculatorMock<ExceptionHandlerMock>::s_result = 10;
	ASSERT_NO_THROW(l_int = l_testObject2 * uint32_t(5));
	multiplicationCheck<uint32_t>(50, l_int.value());
}

TEST_F(SafeIntTest, multiplicationSafeInt)
{
	SafeIntTestu32_t l_int;
   SafeIntTestu32_t l_testObject2(uint32_t(50));
	SafeIntTestu32_t l_testObject3(uint16_t(60));

	CalculatorMock<ExceptionHandlerMock>::reset();
	ExceptionHandlerMock::reset();
	CalculatorMock<ExceptionHandlerMock>::s_result = 30;
	ASSERT_NO_THROW(l_int = l_testObject2 * l_testObject3);
	multiplicationCheck<uint32_t>(50, l_int.value());
}

TEST_F(SafeIntTest, multiplicationSmallerRange)
{
	SafeIntTestu32_t l_int;
	SafeIntTestu32_t l_testObject3(uint16_t(60));
	SafeIntTestu16_t l_testObject4(uint16_t(60));

	CalculatorMock<ExceptionHandlerMock>::reset();
	ExceptionHandlerMock::reset();
	CalculatorMock<ExceptionHandlerMock>::s_result = 20;
	ASSERT_NO_THROW(l_int = l_testObject4 * l_testObject3);
	multiplicationCheck<uint32_t>(60, l_int.value());
}

TEST_F(SafeIntTest, multiplicationSmallerRangeSimpleType)
{
	SafeIntTestu32_t l_int;
	SafeIntTestu16_t l_testObject3(uint16_t(60));

	CalculatorMock<ExceptionHandlerMock>::reset();
	ExceptionHandlerMock::reset();
	CalculatorMock<ExceptionHandlerMock>::s_result = 20;
	ASSERT_NO_THROW(l_int = l_testObject3 * uint32_t(60));
	multiplicationCheck<uint32_t>(60, l_int.value());
}

TEST_F(SafeIntTest, divisionSimpleType)
{
	SafeIntTestu32_t l_int;
   SafeIntTestu32_t l_testObject2(uint32_t(50));
	CalculatorMock<ExceptionHandlerMock>::s_result = 10;
	ASSERT_NO_THROW(l_int = l_testObject2 / uint32_t(5));
	divisionCheck<uint32_t>(50, l_int.value());
	ASSERT_EQ(50, l_testObject2.value());
}

TEST_F(SafeIntTest, divisionSafeInt)
{
	SafeIntTestu32_t l_int;
   SafeIntTestu32_t l_testObject2(uint32_t(50));
	SafeIntTestu32_t l_testObject3(uint32_t(10));
	CalculatorMock<ExceptionHandlerMock>::s_result = 10;
	ASSERT_NO_THROW(l_int = l_testObject2 / l_testObject3);
	divisionCheck<uint32_t>(50, l_int.value());
	ASSERT_EQ(50, l_testObject2.value());
	ASSERT_EQ(10, l_testObject3.value());
}

TEST_F(SafeIntTest, additionAssignSimpleType)
{
   SafeIntTestu32_t l_testObject2(uint32_t(50));
	CalculatorMock<ExceptionHandlerMock>::s_result = 30;
	ASSERT_NO_THROW(l_testObject2 += uint32_t(50));
	additionCheck<uint32_t>(50, l_testObject2.value());
	ASSERT_EQ(30, l_testObject2.value());
}


TEST_F(SafeIntTest, additionAssign)
{
   SafeIntTestu32_t l_testObject2(uint32_t(50));
	SafeIntTestu32_t l_testObject3(uint32_t(10));
	CalculatorMock<ExceptionHandlerMock>::s_result = 30;
	ASSERT_NO_THROW(l_testObject2 += l_testObject3);
	additionCheck<uint32_t>(50, l_testObject2.value());
	ASSERT_EQ(30, l_testObject2.value());
	ASSERT_EQ(10, l_testObject3.value());
}

TEST_F(SafeIntTest, additionSimpleType)
{
	SafeIntTestu32_t l_int;
   SafeIntTestu32_t l_testObject2(uint32_t(50));
	CalculatorMock<ExceptionHandlerMock>::s_result = 30;
	ASSERT_NO_THROW(l_int = l_testObject2 + uint32_t(50));
	additionCheck<uint32_t>(50, l_int.value());
	ASSERT_EQ(50, l_testObject2.value());

}

TEST_F(SafeIntTest, addition)
{
	SafeIntTestu32_t l_int;
   SafeIntTestu32_t l_testObject2(uint32_t(50));
	SafeIntTestu32_t l_testObject3(uint32_t(10));
	CalculatorMock<ExceptionHandlerMock>::s_result = 30;
	ASSERT_NO_THROW(l_int = l_testObject2 + l_testObject3);
	additionCheck<uint32_t>(l_testObject2.value(), l_int.value());
	ASSERT_EQ(50, l_testObject2.value());
	ASSERT_EQ(10, l_testObject3.value());
}

TEST_F(SafeIntTest, additionSmallerRange)
{
	SafeIntTestu32_t l_int;
   SafeIntTestu32_t l_testObject2(uint32_t(50));
	SafeIntTestu16_t l_testObject3(uint16_t(10));
	CalculatorMock<ExceptionHandlerMock>::s_result = 30;
	ASSERT_NO_THROW(l_int = l_testObject3 + l_testObject2);
	additionCheck<uint32_t>(l_testObject2.value(), l_int.value());
	ASSERT_EQ(50, l_testObject2.value());
	ASSERT_EQ(10, l_testObject3.value());
}

TEST_F(SafeIntTest, additionSmallerRangeSimpleType)
{
	SafeIntTestu32_t l_int;
	SafeIntTestu16_t l_testObject3(uint16_t(10));
	CalculatorMock<ExceptionHandlerMock>::s_result = 30;
	ASSERT_NO_THROW(l_int = l_testObject3 + uint32_t(50));
	additionCheck<uint32_t>(l_testObject3.value(), l_int.value());
	ASSERT_EQ(10, l_testObject3.value());
}

TEST_F(SafeIntTest, modulo)
{
    SafeInt<int16_t> l_int1(int16_t(5));
    SafeInt<int8_t> l_int2(int8_t(2));
    ASSERT_TRUE(5%2 == (l_int1%l_int2).value());
    ASSERT_TRUE(5%2 == (l_int1%int16_t(2)).value());

}

TEST_F(SafeIntTest, constCheck)
{
    const SafeInt<uint16_t> l_int1(uint16_t(5));
    const SafeInt<uint8_t> l_int2(uint8_t(5));
    const SafeInt<uint16_t> l_int3 = l_int1;
    ASSERT_TRUE(l_int3 == l_int1);
    SafeInt<uint16_t> l_int4 = l_int1;
    ASSERT_TRUE(l_int4 == l_int1);
    SafeInt<uint16_t> l_int5 = l_int1 * uint8_t(5);
    ASSERT_TRUE(l_int5 == uint8_t(5*5));
    SafeInt<uint16_t> l_int6 = l_int1 * l_int2;
    ASSERT_TRUE(l_int6 == uint8_t(5*5));
}


TEST_F(SafeIntTest, operatorLessThan)
{
    SafeInt<int16_t> l_int1(int16_t(5));
    SafeInt<int8_t> l_int2(int8_t(2));
    ASSERT_TRUE(l_int2 < l_int1);
    ASSERT_FALSE(l_int1 < l_int2);
    ASSERT_TRUE(l_int2 < int8_t(8));
}
TEST_F(SafeIntTest, increment)
{
    SafeInt<int16_t> l_int1(int16_t(5));
    ASSERT_TRUE(l_int1.value() == int16_t(5));
    ASSERT_TRUE(++l_int1 == int16_t(6));

//     SafeInt<uint32_t, OverflowHandlerTest<uint32_t, uint32_t> > l_int2(uint32_t(std::numeric_limits<uint32_t>::max()));
//     OverflowHandlerTest<uint32_t, uint32_t>::currentValue = l_int2.value();
//     OverflowHandlerTest<uint32_t, uint32_t>::operationValue = 0;
//     OverflowHandlerTest<uint32_t, uint32_t>::overflowType = EOverflowType_positive;
//     OverflowHandlerTest<uint32_t, uint32_t>::result = 10;
//     ASSERT_NO_THROW(++l_int2);
//     ASSERT_EQ(1, (OverflowHandlerTest<uint32_t, uint32_t>::handleOverflowCalls));
//     ASSERT_EQ(0, (OverflowHandlerTest<uint32_t, uint32_t>::handleDivByZeroCalls));
//     ASSERT_EQ(10, l_int2.value());
}

TEST_F(SafeIntTest, decrement)
{
//     SafeInt<int16_t> l_int1(int16_t(5));
//     ASSERT_TRUE(l_int1.value() == int16_t(5));
//     ASSERT_TRUE(--l_int1 == int16_t(4));
//     SafeInt<uint32_t, OverflowHandlerTest<uint32_t, uint32_t> > l_int2(uint32_t(0));
//     OverflowHandlerTest<uint32_t, uint32_t>::currentValue = l_int2.value();
//     OverflowHandlerTest<uint32_t, uint32_t>::operationValue = 0;
//     OverflowHandlerTest<uint32_t, uint32_t>::overflowType = EOverflowType_negative;
//     OverflowHandlerTest<uint32_t, uint32_t>::result = 10;
//     ASSERT_NO_THROW(--l_int2);
//     ASSERT_EQ(1, (OverflowHandlerTest<uint32_t, uint32_t>::handleOverflowCalls));
//     ASSERT_EQ(0, (OverflowHandlerTest<uint32_t, uint32_t>::handleDivByZeroCalls));
//     ASSERT_EQ(10, l_int2.value());
}


} // namespace ut
} // namespace safe_int

// kate: indent-mode cstyle; indent-width 3; replace-tabs off; tab-width 3;
